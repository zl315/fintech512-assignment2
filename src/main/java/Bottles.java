package assignment2;

class Bottles {

	public String verse(int verseNumber) {
		if (verseNumber > 2) {
			return verseNumber + " bottles of beer on the wall, " + verseNumber + " bottles of beer.\n"
					+ "Take one down and pass it around, " + (verseNumber - 1) + " bottles of beer on the wall.\n";
		} else if (verseNumber == 2) {
			return verseNumber + " bottles of beer on the wall, " + verseNumber + " bottles of beer.\n"
					+ "Take one down and pass it around, " + (verseNumber - 1) + " bottle of beer on the wall.\n";
		} else if (verseNumber == 1) {
			return "1 bottle of beer on the wall, " + "1 bottle of beer.\n" + "Take it down and pass it around, "
					+ "no more bottles of beer on the wall.\n";
		}
		else if (verseNumber == 0) {
			return "No more bottles of beer on the wall, " + "no more bottles of beer.\n"
					+ "Go to the store and buy some more, " + "99 bottles of beer on the wall.\n";
		}
		return null;
	}


	public String verse(int startVerseNumber, int endVerseNumber) {
		String a = "";
		for(int i= startVerseNumber; i > endVerseNumber; i--){
			a = a+ verse(i) + "\n";
		}
		a = a + verse(endVerseNumber);
		return a;
	}



	public String song() {
		String b = "";
		for(int i = 99; i>0; i--){
			b = b + verse(i) + "\n";
		}
		b = b + verse(0);
		return b;
	}
}